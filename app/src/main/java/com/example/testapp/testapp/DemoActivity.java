package com.example.testapp.testapp;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.testapp.testapp.huddle.StartHuddleIntent;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_demo)
public class DemoActivity extends RoboActivity {

    @InjectView(R.id.article_id_input) private EditText mArticleIdEditText;
    @InjectView(R.id.huddle_button) private Button mHuddleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        mHuddleButton.setOnClickListener(v -> {
            String articleId = mArticleIdEditText.getText().toString();
            startActivity(new StartHuddleIntent(articleId));
        });
    }
}
