package com.example.testapp.testapp;

import com.example.testapp.testapp.huddle.service.FriendsApiManager;
import com.example.testapp.testapp.huddle.service.IFriendsApiManager;
import com.example.testapp.testapp.huddle.service.HuddleServiceProvider;
import com.example.testapp.testapp.huddle.service.IHuddleApiService;
import com.example.testapp.testapp.huddle.settings.IAppSettings;
import com.example.testapp.testapp.huddle.settings.MockAppSettings;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class ServicesModule implements Module {

    private Bus mEventBus = new Bus(ThreadEnforcer.ANY);

    @Override
    public void configure(Binder binder) {
        binder.bind(Bus.class).toInstance(mEventBus);
        binder.bind(IHuddleApiService.class).toProvider(HuddleServiceProvider.class);
        binder.bind(IFriendsApiManager.class).to(FriendsApiManager.class);
        binder.bind(IAppSettings.class).to(MockAppSettings.class);
    }
}
