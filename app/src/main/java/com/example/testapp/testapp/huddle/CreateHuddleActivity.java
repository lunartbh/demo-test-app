package com.example.testapp.testapp.huddle;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.example.testapp.testapp.R;
import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;

@ContentView(R.layout.create_huddle_view)
public class CreateHuddleActivity extends RoboFragmentActivity {

    @Inject Bus mBus;

    private MenuItem mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StartHuddleIntent intent = new StartHuddleIntent(getIntent());

        if (!TextUtils.isEmpty(intent.getArticleId())) {
            Fragment huddleListFragment = HuddleListFragment.newInstance(intent.getArticleId());

            FragmentManager supportFragmentManager = getSupportFragmentManager();
            supportFragmentManager.beginTransaction()
                    .add(R.id.huddle_list_container, huddleListFragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.huddle, menu);
        mMenu = menu.findItem(R.id.create);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mBus.post(new CreateButtonClickedEvent());
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Subscribe
    public void onListSelectionChange(ListSelectionChangeEvent event) {
        mMenu.setEnabled(event.getSelectionCount() > 0);
    }
}
