package com.example.testapp.testapp.huddle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.testapp.testapp.R;
import com.example.testapp.testapp.huddle.models.Friend;
import com.squareup.picasso.Picasso;

public class FriendListItem implements IListViewItem {

    private final Friend mFriend;

    public FriendListItem(Friend friend) {
        mFriend = friend;
    }

    @Override
    public int getViewType() {
        return RowType.CONTENT_ROW.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.friend_list_item, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.setName(mFriend.getName());
        viewHolder.setImage(mFriend.getImageUrl());

        if (mFriend.isPrimary()) {
            viewHolder.setLikes(mFriend.getCategory());
        } else {
            viewHolder.hideLikes();
        }

        if (mFriend.isPrimary() && mFriend.isOnChat()) {
            viewHolder.setStatusIcon(R.drawable.green_dot);
        } else if (mFriend.isPrimary() && !mFriend.isOnChat()) {
            viewHolder.setStatusIcon(R.drawable.grey_dot);
        } else {
            viewHolder.setStatusIconHidden();
        }

        return convertView;
    }

    public Friend getFriend() {
        return mFriend;
    }

    class ViewHolder {
        private final ImageView mFriendImage;
        private final ImageView mStatusView;
        private TextView mNameView;
        private TextView mLikes;

        public ViewHolder(View convertView) {
            mNameView = (TextView) convertView.findViewById(R.id.friend_name);
            mLikes = (TextView) convertView.findViewById(R.id.friend_likes);
            mFriendImage = (ImageView) convertView.findViewById(R.id.friend_image);
            mStatusView = (ImageView) convertView.findViewById(R.id.status_image);
        }

        public void setName(String name) {
            mNameView.setText(name);
        }

        public void setImage(String imageUrl) {
            Picasso.with(mFriendImage.getContext())
                    .load(imageUrl)
                    .error(R.drawable.ic_launcher)
                    .into(mFriendImage);
        }

        public void setStatusIconHidden() {
            mStatusView.setVisibility(View.GONE);
        }

        public void setStatusIcon(int icon) {
            mStatusView.setBackgroundResource(icon);
            mStatusView.setVisibility(View.VISIBLE);
        }

        public void setLikes(String likes) {
            mLikes.setText(String.format("Likes %s", likes));
            mLikes.setVisibility(View.VISIBLE);
        }

        public void hideLikes() {
            mLikes.setVisibility(View.GONE);
        }
    }
}
