package com.example.testapp.testapp.huddle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.google.inject.Inject;

public class FriendsAdapter extends ArrayAdapter<IListViewItem> {

    private final LayoutInflater mLayoutInflater;

    @Inject FriendsAdapter(Context context) {
        super(context, 0);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getItem(position).getView(mLayoutInflater, convertView, parent);
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }
}
