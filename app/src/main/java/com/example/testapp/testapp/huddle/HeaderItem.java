package com.example.testapp.testapp.huddle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.testapp.testapp.R;

public class HeaderItem implements IListViewItem {

    private final String mHeaderTitle;

    public HeaderItem(String headerTitle) {
        mHeaderTitle = headerTitle;
    }

    @Override
    public int getViewType() {
        return RowType.HEADER_ROW.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.header_row, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.setTitle(mHeaderTitle);

        return convertView;
    }

    private class ViewHolder {
        private TextView mHeaderTextView;

        public ViewHolder(View convertView) {
            mHeaderTextView = (TextView) convertView.findViewById(R.id.header_title);
        }

        public void setTitle(String headerTitle) {
            mHeaderTextView.setText(headerTitle);
        }
    }
}
