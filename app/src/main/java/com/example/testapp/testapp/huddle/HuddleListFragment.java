package com.example.testapp.testapp.huddle;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.example.testapp.testapp.R;
import com.example.testapp.testapp.huddle.models.Friend;
import com.example.testapp.testapp.huddle.service.IFriendsApiManager;
import com.example.testapp.testapp.huddle.summary.CreateHuddleIntent;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class HuddleListFragment extends RoboFragment {
    private static final String ARTICLE_ID = "com.example.testapp.testapp.huddle.ARTICLE_ID";

    @InjectView(R.id.huddle_list) ListView mListView;
    @InjectView(R.id.loading_spinner) private ProgressBar mLoadingSpinner;

    @Inject private Bus mBus;
    @Inject private FriendsAdapter mAdapter;
    @Inject private IFriendsApiManager mHuddleFriendsApiManager;

    private String mArticleId;

    public static Fragment newInstance(String articleId) {
        HuddleListFragment fragment = new HuddleListFragment();

        Bundle bundle = new Bundle();
        bundle.putString(ARTICLE_ID, articleId);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mArticleId = getArguments().getString(ARTICLE_ID, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.huddle_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener((adapterView, view1, i, l) ->
                mBus.post(new ListSelectionChangeEvent(getSelectedFriends().size())));
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBus.register(this);
        updateFriendsList();
    }

    private List<Friend> getSelectedFriends() {
        List<Friend> selectedFriends = new ArrayList<>();
        SparseBooleanArray checked = mListView.getCheckedItemPositions();
        for (int i = 0; i < mListView.getCount(); i++) {
            if (checked.get(i)) {
                FriendListItem friendListItem = (FriendListItem) mAdapter.getItem(i);
                selectedFriends.add(friendListItem.getFriend());
            }
        }

        return selectedFriends;
    }

    private void updateFriendsList() {
        mLoadingSpinner.setVisibility(View.VISIBLE);

        mHuddleFriendsApiManager.getFriends(mArticleId, new IFriendsApiManager.OnFriendsLoadedCallback() {
            @Override
            public void onFriendsLoaded(List<Friend> friends) {
                List<IListViewItem> adapterList = createUpAdapterList(friends);

                mLoadingSpinner.setVisibility(View.GONE);
                mAdapter.clear();
                mAdapter.addAll(adapterList);
            }

            @Override
            public void onFailure() {
                mLoadingSpinner.setVisibility(View.GONE);
            }
        });
    }

    private List<IListViewItem> createUpAdapterList(List<Friend> friends) {
        ImmutableList<Friend> otherFriends = FluentIterable.from(friends).filter(f -> "other".equals(f.getCategory())).toList();
        ImmutableList<Friend> allFriends = FluentIterable.from(friends).filter(f -> "all".equals(f.getCategory())).toList();
        ImmutableList<Friend> recommendedFriends = FluentIterable.from(friends).filter(f -> !allFriends.contains(f) && !otherFriends.contains(f)).toList();

        return Lists.newArrayList(Iterables.concat(
                    generateListSection(recommendedFriends, getString(R.string.recommended_title)),
                    generateListSection(otherFriends, getString(R.string.other_title)),
                    generateListSection(allFriends, getString(R.string.all_title)))
              );
    }

    private List<IListViewItem> generateListSection(List<Friend> friends, String header) {
        List<IListViewItem> listViewItems = new ArrayList<>();

        if (friends.size() > 0) {
            listViewItems.add(new HeaderItem(header));
            for (Friend friend : friends) {
                listViewItems.add(new FriendListItem(friend));
            }
        }

        return listViewItems;
    }

    @Subscribe
    public void onCreateButtonClickedEvent(CreateButtonClickedEvent event) {
        if (isAdded()) {
            startActivity(new CreateHuddleIntent(getSelectedFriends()));
            getActivity().finish();
        }
    }
}
