package com.example.testapp.testapp.huddle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface IListViewItem {
    public int getViewType();
    View getView(LayoutInflater inflater, View convertView, ViewGroup parent);
}
