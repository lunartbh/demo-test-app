package com.example.testapp.testapp.huddle;

public class ListSelectionChangeEvent {
    private int mSelectionCount;

    public ListSelectionChangeEvent(int count) {
        mSelectionCount = count;
    }

    public int getSelectionCount() {
        return mSelectionCount;
    }
}
