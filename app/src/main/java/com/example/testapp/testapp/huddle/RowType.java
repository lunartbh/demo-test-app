package com.example.testapp.testapp.huddle;

public enum RowType {
    HEADER_ROW, CONTENT_ROW
}
