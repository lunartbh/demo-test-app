package com.example.testapp.testapp.huddle;

import android.content.Intent;

public class StartHuddleIntent extends Intent {
    public static final String ACTION = "com.example.testapp.testapp.huddle.START_CREATE_HUDDLE";
    private static final String ARTICLE_ID_KEY = "com.example.testapp.testapp.huddle.ARTICLE_ID_KEY";

    private String mArticleId;

    public StartHuddleIntent(String articleId) {
        setAction(ACTION);
        putExtra(ARTICLE_ID_KEY, articleId);
    }

    public StartHuddleIntent(Intent intent) {
        super(intent);
        mArticleId = intent.getStringExtra(ARTICLE_ID_KEY);
    }

    public String getArticleId() {
        return mArticleId;
    }
}
