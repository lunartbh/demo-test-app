package com.example.testapp.testapp.huddle.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Friend implements Parcelable {
    @SerializedName("id") private String mId;
    @SerializedName("name") private String mName;
    @SerializedName("image") private String mImage;
    @SerializedName("primary") private boolean mPrimary;
    @SerializedName("facebookid") private String mFacebookid;
    @SerializedName("chat") private boolean mChat;

    private String mCategory;

    public Friend(Parcel parcel) {
        mId = parcel.readString();
        mName = parcel.readString();
        mImage = parcel.readString();
        mPrimary = parcel.readInt() > 0;
        mFacebookid = parcel.readString();
        mChat = parcel.readInt() > 0;
    }

    public boolean isOnChat() {
        return mChat;
    }

    public boolean isPrimary() {
        return mPrimary;
    }

    public String getImageUrl() {
        return mImage;
    }

    public String getName() {
        return mName;
    }

    public void setCategory(String Category) {
        mCategory = Category;
    }

    public String getCategory() {
        return mCategory;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mName);
        parcel.writeString(mImage);
        parcel.writeInt(mPrimary ? 1 : 0);
        parcel.writeString(mFacebookid);
        parcel.writeInt(mChat ? 1 : 0);
    }

    public static Creator<Friend> CREATOR = new Creator<Friend>() {
        @Override
        public Friend createFromParcel(Parcel parcel) {
            return new Friend(parcel);
        }

        @Override
        public Friend[] newArray(int i) {
            return new Friend[i];
        }
    };
}
