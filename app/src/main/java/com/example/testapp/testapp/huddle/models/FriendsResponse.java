package com.example.testapp.testapp.huddle.models;

import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FriendsResponse {
    @SerializedName("response")
    Map<String, List<Friend>> mFriends;

    public FriendsResponse() {
        this.mFriends = new HashMap<>();
    }

    public Map<String, List<Friend>> getFriends() {
        return mFriends;
    }
}
