package com.example.testapp.testapp.huddle.service;

import com.example.testapp.testapp.huddle.models.Friend;
import com.example.testapp.testapp.huddle.models.FriendsResponse;
import com.example.testapp.testapp.huddle.settings.IAppSettings;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FriendsApiManager implements IFriendsApiManager {

    private final IHuddleApiService mApiService;
    private final IAppSettings mAppSettings;

    @Inject FriendsApiManager(IHuddleApiService apiService, IAppSettings appSettings) {
        mApiService = apiService;
        mAppSettings = appSettings;
    }

    @Override
    public void getFriends(String mArticleId, OnFriendsLoadedCallback callback) {
        Map<String, String> apiQuery = new HashMap<>();
            apiQuery.put("app-id", mAppSettings.getAppId());
            apiQuery.put( "app-version", mAppSettings.getAppVersion());
            apiQuery.put( "app-platform", "android");
            apiQuery.put("include-all", mAppSettings.shouldIncludeAll());
            apiQuery.put("itemid", mArticleId);
            apiQuery.put("auth-fanatix-id", mAppSettings.getAuthId());
            apiQuery .put("auth-fanatix-token", mAppSettings.getAuthToken());

        mApiService.getFriends(apiQuery, new Callback<FriendsResponse>() {
            @Override
            public void success(FriendsResponse friendsResponse, Response response) {
                Map<String, List<Friend>> friendsMap = friendsResponse.getFriends();

                List<Friend> friendsWithCategory = new ArrayList<>();

                for (String key : friendsMap.keySet()) {
                    List<Friend> friends = friendsMap.get(key);

                    for (Friend friend : friends) {
                        friend.setCategory(key);
                        friendsWithCategory.add(friend);
                    }
                }

                callback.onFriendsLoaded(friendsWithCategory);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onFailure();
            }
        });
    }
}
