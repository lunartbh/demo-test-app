package com.example.testapp.testapp.huddle.service;

import com.example.testapp.testapp.huddle.settings.IAppSettings;
import com.google.inject.Inject;
import com.google.inject.Provider;
import retrofit.RestAdapter;

public class HuddleServiceProvider implements Provider<IHuddleApiService> {
    private final RestAdapter restAdapter;

    @Inject public HuddleServiceProvider(IAppSettings appSettings) {
        restAdapter = new RestAdapter.Builder()
                            .setEndpoint(appSettings.getApiUrl())
                            .build();
    }

    @Override
    public IHuddleApiService get() {
        return restAdapter.create(IHuddleApiService.class);
    }
}
