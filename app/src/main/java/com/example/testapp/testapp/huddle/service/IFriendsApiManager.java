package com.example.testapp.testapp.huddle.service;

import com.example.testapp.testapp.huddle.models.Friend;
import java.util.List;

public interface IFriendsApiManager {
    void getFriends(String mArticleId, OnFriendsLoadedCallback onFriendsLoadedCallback);

    public interface OnFriendsLoadedCallback {
        void onFriendsLoaded(List<Friend> friends);
        void onFailure();
    }

}
