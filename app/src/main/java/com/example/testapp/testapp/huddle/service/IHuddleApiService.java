package com.example.testapp.testapp.huddle.service;

import com.example.testapp.testapp.huddle.models.FriendsResponse;

import java.util.Map;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.QueryMap;

public interface IHuddleApiService {
    @GET("/news/item-friends")
    void getFriends(@QueryMap Map<String, ?> query, Callback<FriendsResponse> response);
}
