package com.example.testapp.testapp.huddle.settings;

public interface IAppSettings {
    String getApiUrl();
    String getAppId();
    String getAppVersion();
    String getAuthId();
    String getAuthToken();
    String shouldIncludeAll();
}
