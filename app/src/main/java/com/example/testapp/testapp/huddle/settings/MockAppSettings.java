package com.example.testapp.testapp.huddle.settings;

public class MockAppSettings implements IAppSettings {
    @Override
    public String getApiUrl() {
        return "http://dev.staging.fanatix.com/app-api/1.3";
    }

    @Override
    public String getAppId() {
        return "cos-iphone";
    }

    @Override
    public String getAppVersion() {
        return "1.2.3AT";
    }

    @Override
    public String getAuthId() {
        return "50f82e1d4a8b519d6d000069";
    }

    @Override
    public String getAuthToken() {
        return "5fd203caf74e219f585067338b5afae3";
    }

    @Override
    public String shouldIncludeAll() {
        return "true";
    }
}
