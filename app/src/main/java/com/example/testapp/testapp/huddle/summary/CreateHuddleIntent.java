package com.example.testapp.testapp.huddle.summary;

import android.content.Intent;
import com.example.testapp.testapp.huddle.models.Friend;
import java.util.ArrayList;
import java.util.List;

public class CreateHuddleIntent extends Intent {
    private static final String HUDDLE_FRIENDS_KEY = "com.example.testapp.testapp.huddle.HUDDLE_FREINDS_KEY";
    public static final String ACTION = "com.example.testapp.testapp.huddle.CREATE";

    private final ArrayList<Friend> mFriendsList;

    public CreateHuddleIntent(List<Friend> friends) {
        setAction(ACTION);
        mFriendsList = new ArrayList<>(friends);
        putParcelableArrayListExtra(HUDDLE_FRIENDS_KEY, mFriendsList);
    }

    public CreateHuddleIntent(Intent intent) {
        mFriendsList = intent.getParcelableArrayListExtra(HUDDLE_FRIENDS_KEY);
    }

    public List<Friend> getFriends() {
        return mFriendsList;
    }
}
