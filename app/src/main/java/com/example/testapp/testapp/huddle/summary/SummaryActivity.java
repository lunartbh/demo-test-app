package com.example.testapp.testapp.huddle.summary;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.testapp.testapp.R;
import com.example.testapp.testapp.huddle.models.Friend;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;

import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.summary)
public class SummaryActivity extends RoboActivity {

    @InjectView(R.id.primary_summary) TextView mPrimarySummarView;
    @InjectView(R.id.non_primary_summary) TextView mNonPrimarySummarView;
    @InjectView(R.id.non_summar_title) TextView mNonPrimarySummarTitleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CreateHuddleIntent intent = new CreateHuddleIntent(getIntent());

        List<Friend> friends = intent.getFriends();

        ImmutableList<Friend> nonPrimaryFriends = FluentIterable.from(friends).filter(f -> !f.isPrimary()).toList();

        if (nonPrimaryFriends.size() > 0) {
            mNonPrimarySummarView.setVisibility(View.VISIBLE);
            mNonPrimarySummarTitleView.setVisibility(View.VISIBLE);
            mNonPrimarySummarView.setText(toSummaryString(nonPrimaryFriends));
        }

        mPrimarySummarView.setText(toSummaryString(friends));
    }

    private String toSummaryString(List<Friend> friends) {
        StringBuilder friendsConfirmationList = new StringBuilder();

        for (Friend friend : friends) {
            friendsConfirmationList.append(friend.getName());
            friendsConfirmationList.append(", ");
        }

        return friendsConfirmationList.toString();
    }
}
